package ReadString;
import java.lang.Object.*;


/**
 * Created by victoriali on 22/10/2016.
 */
public class TypeChecker
{
    //check if the string is number
    public boolean isNumeric(String str)
    {
        try {
            int newInt = Integer.parseInt(str);
            //System.out.println(newInt);
            return true;
        }
        catch(Exception NotNumberException)
        {
            return false;

        }

    }

    //Check if the string array contains a certain string

    public boolean containAString(String[] list,String elem){
        for(String word:list)
        {
            if(elem.equals(word))
            {
                return true;
            }
        }
        return false;
    }

    public boolean checkIfMonthWeekValid(String[] month, String[] weeks,
                                         String partOne, String partTwo)
    {
        boolean monthIsCorrect = containAString(month, partOne);
        boolean weekIsCorrect = containAString(weeks, partTwo);

        if(monthIsCorrect && weekIsCorrect)
        {
            return true;
        }
        else {
            return false;
        }
    }





}
