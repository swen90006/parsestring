package ReadString;

public class Calculator {
    //calculate average wage

    TypeChecker check = new TypeChecker();

    public String getMonth(String[] month, String partOne) {
        if (check.containAString(month, partOne)) {
            return partOne;
        } else {
            return "invalid month,";
        }
    }

    public String getWeek(String[] weeks, String partTwo) {
        if (check.containAString(weeks, partTwo)) {
            return partTwo;
        }
        return "invalid week,";

    }

    public String calculateAvgWage(String total, String employees) throws DivisionByZeroException {

        boolean totalIsNumber = check.isNumeric(total);
        // System.out.print("totalIsNumber:" + totalIsNumber);
        boolean employeesIsNumber = check.isNumeric(employees);
        //System.out.print("empNumberr:" + employeesIsNumber);
        try {
            if (totalIsNumber && employeesIsNumber) {
                //System.out.println("WHY IS IT COMING IN HERE");
                if(Integer.parseInt(employees) == 0)
                {
                    throw new DivisionByZeroException();
                }

                if(Integer.parseInt(employees) < 0)
                {
                    throw new EmployeesCantBeLessThanZeroException();
                }

                int t = Integer.valueOf(total);
                int e = Integer.valueOf(employees);
                int result = t / e;
                String avg = String.valueOf(result);
                return avg;
            } else {
                return " Average cannot be calculated, invalid input";

                }
        }
        catch(Exception e)
        {
            System.out.print(e.toString());
        }
        return "Error";
    }

    //calculate total cost of operation
    public String calculateCost(String total, String otherCost) {
        boolean totalIsNumber = check.isNumeric(total);
        boolean otherCostIsNumber = check.isNumeric(otherCost);

        if (totalIsNumber && otherCostIsNumber) {
            int t = Integer.valueOf(total);
            int c = Integer.valueOf(otherCost);
            int result = t + c;
            String cost = String.valueOf(result);
            return cost;
        } else {
            return "Total cost cannot be calculated, invalid input,";

        }
    }

    //calculate profit
    public String calculateProfit(String revenue, String total, String otherCost) {

        boolean totalIsNumber = check.isNumeric(total);
        boolean revenueIsNumber = check.isNumeric(revenue);
        boolean otherCostIsNumber = check.isNumeric(otherCost);

        if (totalIsNumber && revenueIsNumber && otherCostIsNumber) {
            int r = Integer.valueOf(revenue);
            int t = Integer.valueOf(total);
            int c = Integer.valueOf(otherCost);
            int result = r - t - c;
            String profit = String.valueOf(result);
            return profit;
        }
        else {;
        }
        return "Profit cannot be calculated, invalid input";
}


}
