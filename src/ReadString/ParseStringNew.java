package ReadString;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Created by victoriali on 22/10/2016.
 */
public class ParseStringNew {
    public HashMap<String, String> exactMatched;
    String[] month = Input.MONTHS;
    String[] weeks = Input.WEEKS;
    Calculator calculator = new Calculator();
    TypeChecker check = new TypeChecker();

    public void handleInput(String file) throws Throwable {
        String[] months = Input.MONTHS;
        String[] weeks = Input.WEEKS;
        Calculator calculator = new Calculator();
        ReadFile read = new ReadFile();
        ArrayList<String> info = new ArrayList<String>();
        String[] words;

        //read the input file by lines, store the information in an ArrayList info
        info = read.readFileByLines(file);

        exactMatched = new HashMap<String, String>();
        int count = 1;

        //check every string in the info,calculate results
        for (int t = 0; t < info.size(); t++) {

            System.out.println(count);
            words = read.splitStringByComma(info.get(t));
           
            try {
                if (info.get(t).isEmpty()) {
                    throw new EmptyStringException();
                } else if (!(words.length == 6)) {
                    throw new ShortInputException();
                }
            }
            catch(Exception e)
            {
                System.out.print(e);
                continue;
            }
            //if the first section of the string matches the format "month-week", proceed to the calculation part

            try {
                String partOne = words[0];
                String partTwo = words[1];

                //calculate average wage, total cost, profit
                String theMonth = calculator.getMonth(months, partOne);
                String theWeek = calculator.getWeek(weeks, partTwo);
                String avgWage = calculator.calculateAvgWage(words[2], words[3]);
                String totalCost = calculator.calculateCost(words[2], words[4]);
                String profit = calculator.calculateProfit(words[5], words[2], words[4]);
                String validOutput = "Month: " + theMonth + " Week: " + theWeek + " Average Wage:" + avgWage + ", Total Cost:" + totalCost + " Profit:" + profit;
                exactMatched.put(info.get(t), validOutput);
                System.out.println("[" + info.get(t)+ "]" + "  "+ validOutput);

            } catch (Exception InvalidInputException) {

                System.out.println("Invalid input, please check your text file for corrupt data");
            }

            count++;


        }


        // print the result
        PrintWriter writer = new PrintWriter("result.txt", "UTF-8");


        Iterator<Map.Entry<String, String>> iter2 = exactMatched.entrySet().iterator();
        while (iter2.hasNext())

        {
            Map.Entry entry = (Map.Entry) iter2.next();
            Object key = entry.getKey();
            writer.println(entry.getKey() + entry.getValue().toString());

            //System.out.println(entry.getKey() + entry.getValue().toString());


        }

        writer.close();

    }


}
