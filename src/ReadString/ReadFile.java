package ReadString;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class ReadFile {

	 public ArrayList<String> readFileByLines(String fileName) {
	        File file = new File(fileName);
	        BufferedReader reader = null;
	        ArrayList<String> titles =new ArrayList<String>();
	        try {
	          
	            reader = new BufferedReader(new FileReader(file));
	            String tempString = null;
	           
	           
	            while ((tempString = reader.readLine()) != null) {
	                titles.add(tempString);
	       
	            }
	           
	            reader.close();
	             
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (reader != null) {
	                try {
	                    reader.close();
	                } catch (IOException e1) {
	                }
	            }
	        }
			return titles;
	    }
	 
	 public ArrayList<String> readFileByWord(String fileName) throws IOException{
		 File file = new File(fileName);
	        BufferedReader reader = null;
	        ArrayList<String> words =new ArrayList<String>();
	        try {
	          
	            reader = new BufferedReader(new FileReader(file));
	            String tempString = null;
	           
	           
	            while ((tempString = reader.readLine()) != null) {
	            	String [] tokens = tempString.split(",");
	   		     for(String t: tokens){
	   		    	 words.add(t);
	   		     }
	       
	            }
	           
	            reader.close();
	             
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (reader != null) {
	                try {
	                    reader.close();
	                } catch (IOException e1) {
	                }
	            }
	        }
			return words;
			
		
	 }
	
	 public ArrayList<String> readFileBySentence(String fileName) throws IOException{
		 File file = new File(fileName);
	        BufferedReader reader = null;
	        ArrayList<String> sentences =new ArrayList<String>();
	        try {
	          
	            reader = new BufferedReader(new FileReader(file));
	            String tempString = null;
	           
	           
	            while ((tempString = reader.readLine()) != null) {
	            	String [] tokens = tempString.split(",+|\\.+|\\?+|\\!");
	   		     for(String t: tokens){
	   		    	 sentences.add(t);
	   		     }
	       
	            }
	           
	            reader.close();
	             
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (reader != null) {
	                try {
	                    reader.close();
	                } catch (IOException e1) {
	                }
	            }
	        }
			return sentences;
			
		
	 }

	 public String[] splitStringByComma(String str)
	 {
	 	String[] wordsList = str.split(",");
	 	return wordsList;
	 }
}
